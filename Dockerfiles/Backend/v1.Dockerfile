FROM node:14

LABEL Description="Express NodeJS backend service for DevOps exam"

ENV DOCKERFILE_VERSION="v1"

ENV API_MESSAGE="API is working properly"

EXPOSE 9000

RUN apt-get update

RUN apt-get install iputils-ping

RUN apt-get clean

COPY ./backend /opt/backend

WORKDIR /opt/backend

RUN npm install

RUN npm cache clean --force 

ENTRYPOINT [ "npm" ]

CMD [ "start" ]