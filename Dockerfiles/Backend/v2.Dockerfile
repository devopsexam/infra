FROM node:14

LABEL Description="Express NodeJS backend service for DevOps exam"

ENV DOCKERFILE_VERSION="v3" \
    API_MESSAGE="API is working properly"

EXPOSE 9000

RUN apt-get update && \
    apt-get install iputils-ping && \
    apt-get clean

COPY ./backend /opt/backend

WORKDIR /opt/backend

RUN npm install && \
    npm cache clean --force 

ENTRYPOINT [ "npm" ]

CMD [ "start" ]