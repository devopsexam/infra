FROM node:14

LABEL Description="Express NodeJS backend service for DevOps exam"

ENV DOCKERFILE_VERSION="v3" \
    API_MESSAGE="API is working properly"

EXPOSE 9000

COPY ./backend /opt/backend

WORKDIR /opt/backend

RUN npm install && \
    npm cache clean --force && \
    apt-get update && \
    apt-get install iputils-ping && \
    apt-get clean

ENTRYPOINT [ "npm" ]

CMD [ "start" ]