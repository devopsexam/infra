FROM node:14

LABEL Description="Express NodeJS frontend service for DevOps exam"

ENV DOCKERFILE_VERSION="v1" \
    API_IP="0.0.0.0" \
    API_PORT="9000"

EXPOSE 3000

RUN apt-get update && \
    apt-get install iputils-ping && \
    apt-get clean

COPY ./frontend /opt/frontend

WORKDIR /opt/frontend

RUN npm install && \
    npm cache clean --force 

ENTRYPOINT [ "npm" ]

CMD [ "start" ]