FROM node:14

LABEL Description="Express NodeJS frontend service for DevOps exam"

ENV DOCKERFILE_VERSION="v3"
ENV API_IP="0.0.0.0"
ENV API_PORT="9000"

EXPOSE 3000

COPY ./frontend /opt/frontend

WORKDIR /opt/frontend

RUN apt-get update

RUN apt-get install iputils-ping

RUN apt-get clean

RUN npm install

RUN npm cache clean --force 

ENTRYPOINT [ "npm" ]

CMD [ "start" ]